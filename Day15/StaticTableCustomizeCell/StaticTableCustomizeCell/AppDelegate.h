//
//  AppDelegate.h
//  StaticTableCustomizeCell
//
//  Created by techmaster on 6/24/13.
//  Copyright (c) 2013 Techmaster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
