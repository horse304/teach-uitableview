//
//  Person.h
//  CustomizeTableCell
//
//  Created by techmaster on 11/27/12.
//  Copyright (c) 2012 TechMaster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIImage *photo;
- (id) init: (NSString *) name
  withTitle: (NSString *) title
   andPhoto: (UIImage *) photo;
@end
