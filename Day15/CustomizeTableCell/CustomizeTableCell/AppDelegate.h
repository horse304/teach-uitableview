//
//  AppDelegate.h
//  CustomizeTableCell
//
//  Created by techmaster on 11/27/12.
//  Copyright (c) 2012 TechMaster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
