//
//  main.m
//  CustomizeTableCell
//
//  Created by techmaster on 11/27/12.
//  Copyright (c) 2012 TechMaster. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
