//
//  Person.m
//  CustomizeTableCell
//
//  Created by techmaster on 11/27/12.
//  Copyright (c) 2012 TechMaster. All rights reserved.
//

#import "Person.h"

@implementation Person
- (id) init: (NSString *) name
  withTitle: (NSString *) title
   andPhoto: (UIImage *) photo
{
    if (self = [super init]){
        _name = name;
        _title = title;
        _photo = photo;
    }
    return self;
}
@end
