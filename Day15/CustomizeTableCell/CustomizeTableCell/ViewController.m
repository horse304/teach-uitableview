//
//  ViewController.m
//  CustomizeTableCell
//
//  Created by techmaster on 11/27/12.
//  Copyright (c) 2012 TechMaster. All rights reserved.
//

#import "ViewController.h"
#import "Person.h"
#import "OddCell.h"
#import "EvenCell.h"
@interface ViewController ()

@end

@implementation ViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!_data)
    {
        _data = [NSMutableArray new];
        [_data addObject:[[Person alloc] init:@"Cuong" withTitle:@"Developer" andPhoto:[UIImage imageNamed:@"cuong.jpg"]]];
        
        [_data addObject:[[Person alloc] init:@"John Lenon" withTitle:@"Singer" andPhoto:[UIImage imageNamed:@"johnlenon.jpg"]]];
        
        [_data addObject:[[Person alloc] init:@"Barak Obama" withTitle:@"President" andPhoto:[UIImage imageNamed:@"obama.jpg"]]];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *OddCellID = @"OddCell";
    static NSString *EvenCellID = @"EvenCell";
   
    Person *person = [_data objectAtIndex:indexPath.row];
 
    if (indexPath.row %2 ==0){
        EvenCell* cell = [tableView dequeueReusableCellWithIdentifier:EvenCellID forIndexPath:indexPath];
        cell.photo.image = person.photo;
        cell.name.text = person.name;
        cell.title.text = person.title;
        return cell;
    } else {
        OddCell* cell = [tableView dequeueReusableCellWithIdentifier:OddCellID forIndexPath:indexPath];
        cell.photo.image = person.photo;
        cell.name.text = person.name;
        cell.title.text = person.title;
        return cell;
    }
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

@end
