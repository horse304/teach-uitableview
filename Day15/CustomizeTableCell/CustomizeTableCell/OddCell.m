//
//  OddCell.m
//  CustomizeTableCell
//
//  Created by techmaster on 11/27/12.
//  Copyright (c) 2012 TechMaster. All rights reserved.
//

#import "OddCell.h"

@implementation OddCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
