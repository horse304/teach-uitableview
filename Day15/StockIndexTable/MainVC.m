//
//  MainVC.m
//  StockIndexTable
//
//  Created by Techmaster on 8/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainVC.h"
#import "ChartView.h"

#define NUMBER_DAYS 10
#define STOCK_KEY @"name"
#define STOCK_VALS @"stock"
#define MAX_STOCK 80

@interface MainVC ()
@property (nonatomic, strong) NSMutableArray *data;
@end

@implementation MainVC
@synthesize data = _data;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
    
}
// Khởi tạo dữ liệu là một mảng. Mỗi phần tử là một dictionary có 1 text và 1 array
- (void) initData
{
    _data = [NSMutableArray new];
    [_data addObject: [[NSDictionary alloc] initWithObjectsAndKeys: @"Apple", STOCK_KEY, [self generateStock:NUMBER_DAYS], STOCK_VALS, nil]];
    
    [_data addObject: [[NSDictionary alloc] initWithObjectsAndKeys: @"Microsoft", STOCK_KEY, [self generateStock:NUMBER_DAYS], STOCK_VALS, nil]];
    
    [_data addObject: [[NSDictionary alloc] initWithObjectsAndKeys: @"Google", STOCK_KEY, [self generateStock:NUMBER_DAYS], STOCK_VALS, nil]];
    
    [_data addObject: [[NSDictionary alloc] initWithObjectsAndKeys: @"Vinashin", STOCK_KEY, [self generateStock:NUMBER_DAYS], STOCK_VALS, nil]];
}
// Duc Anh viet
- (NSArray*) generateStock: (int) numberDays
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (int i = 0; i<numberDays; i++) {        
        [result addObject: [[NSNumber alloc] initWithInt:rand() % MAX_STOCK]];
    }
    return [NSArray arrayWithArray:result];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    self.data = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    return cell;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *companyStock = [self.data objectAtIndex:indexPath.row];
    cell.textLabel.text = [companyStock objectForKey:STOCK_KEY];
    
    ChartView *chartView = [ChartView layer];
    
    [chartView initWithData:[companyStock objectForKey:STOCK_VALS] 
                   andFrame: CGRectMake(100, 0, cell.bounds.size.width-100, cell.bounds.size.height)];
    
    [cell.layer addSublayer:chartView]; 
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
