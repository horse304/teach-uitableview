//
//  ViewController.h
//  TableStoryBoard
//
//  Created by techmaster on 1/25/13.
//  Copyright (c) 2013 TechMaster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic, strong) NSString *text;
@property (weak, nonatomic) IBOutlet UILabel *myLabel;

@end
