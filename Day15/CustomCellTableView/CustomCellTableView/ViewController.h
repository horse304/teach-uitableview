//
//  ViewController.h
//  CustomCellTableView
//
//  Created by techmaster on 11/27/12.
//  Copyright (c) 2012 TechMaster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UITableViewController <UITextFieldDelegate>

@end
