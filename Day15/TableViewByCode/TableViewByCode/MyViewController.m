//
//  MyViewController.m
//  TableViewByCode
//
//  Created by techmaster on 11/24/12.
//  Copyright (c) 2012 TechMaster. All rights reserved.
//

#import "MyViewController.h"
#import "DetailViewController.h"
@interface MyViewController ()
{
    DetailViewController *_detailVC;
}

@end

@implementation MyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Some good men";
        [self initData];
        CGSize size = self.view.bounds.size;
        UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,0,size.width,44)];
        searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        CGRect tableViewRect = CGRectMake(0, 44, size.width, size.height-44);
        
        self.tableView = [[UITableView alloc] initWithFrame:tableViewRect style:UITableViewStylePlain];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.autoresizingMask = UIViewAutoresizingNone | UIViewAutoresizingFlexibleWidth;
        
        
        [self.view addSubview:searchBar];
        [self.view addSubview: self.tableView];
        
        
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editTable)];
        
        UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reloadTable)];
        
        self.navigationItem.rightBarButtonItems = @[reloadButton, editButton]; //Syntax cua iOS 6.
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Data
//prepare model data for table view to display
- (void) initData
{
    if (!self.data){
        self.data = [NSMutableArray new];
        [self.data addObject: [[Person alloc] init:@"Bill Gates"
                                         withTitle:@"ex-President of Microsoft"
                                          andPhoto:[UIImage imageNamed:@"bill0.jpg"]]];
        
        [self.data addObject: [[Person alloc] init:@"Mark Zuckerberg"
                                         withTitle:@"CEO of Facebook"
                                          andPhoto:[UIImage imageNamed:@"zuck0.jpg"]]];
        
        [self.data addObject: [[Person alloc] init:@"Steve Jobs"
                                         withTitle:@"ex-President of Apple"
                                          andPhoto:[UIImage imageNamed:@"stevejobs0.jpg"]]];
        
        [self.data addObject: [[Person alloc] init:@"Truong Gia Binh"
                                         withTitle:@"CEO of FPT"
                                          andPhoto:[UIImage imageNamed:@"binh0.jpg"]]];
        
    }
}
#pragma mark - Actions on TableView
- (void) editTable
{
    if (self.tableView.editing) {
        [self.tableView setEditing:NO animated:YES];
    } else {
        [self.tableView setEditing:YES animated:YES];
    }
}

- (void) reloadTable
{
    [self.tableView reloadData];
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data count];
}
//Trả về từng cell/row của UITableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *result = nil;
    static NSString *TableViewCellIdentifier = @"MyCells";   //Đây là một static string để đánh dấu CELL
    result = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (result == nil){
        result = [[UITableViewCell alloc]
                  initWithStyle:UITableViewCellStyleSubtitle
                  reuseIdentifier:TableViewCellIdentifier];
    }
    
    Person *person = (Person *)[self.data objectAtIndex:indexPath.row];
    result.textLabel.text = person.fullName;
    result.detailTextLabel.text = person.title;
    result.imageView.image = person.photo;
    result.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    return result;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (editingStyle == UITableViewCellEditingStyleDelete){
        
        //Phải xoá dữ liệu trước rồi xoá row ở bảng
        [self.data removeObjectAtIndex:indexPath.row];

        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected row at index: %d", indexPath.row);
    if (!_detailVC)
    {
        _detailVC = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
        
    }
    _detailVC.person = self.data[indexPath.row];
    [self.navigationController pushViewController:_detailVC animated:YES];
}
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath
      toIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSLog(@"From Row %d to Row %d", sourceIndexPath.row, destinationIndexPath.row);
   /* Hung
    if (destinationIndexPath.row < sourceIndexPath.row) {
        [self.data insertObject:self.data[sourceIndexPath.row] atIndex:destinationIndexPath.row];
        [self.data removeObjectAtIndex:sourceIndexPath.row + 1];
    } else {
        [self.data insertObject:self.data[sourceIndexPath.row] atIndex:destinationIndexPath.row+1];
        [self.data removeObjectAtIndex:sourceIndexPath.row];
    }*/
    //Thang
    Person *person = [self.data objectAtIndex:sourceIndexPath.row];
    [self.data removeObjectAtIndex:sourceIndexPath.row];
    [self.data insertObject:person atIndex:destinationIndexPath.row];
    
    
}

@end
