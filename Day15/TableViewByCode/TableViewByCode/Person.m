//
//  Person.m
//  TableViewByCode
//
//  Created by techmaster on 11/24/12.
//  Copyright (c) 2012 TechMaster. All rights reserved.
//

#import "Person.h"

@implementation Person

- (id) init: (NSString *) fullName withTitle :(NSString *) title andPhoto: (UIImage *)photo
{
    self = [super init];
    if (self) {
        _fullName = fullName;
        _title = title;
        _photo = photo;
    }
    return self;
}

- (NSString* ) description
{
    return _fullName;
}
@end
