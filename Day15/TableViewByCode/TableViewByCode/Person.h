//
//  Person.h
//  TableViewByCode
//
//  Created by techmaster on 11/24/12.
//  Copyright (c) 2012 TechMaster. All rights reserved.
//

#import <Foundation/Foundation.h>
//3. Create Model Item
@interface Person : NSObject
@property (nonatomic, strong) NSString* fullName;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) UIImage * photo;

- (id) init: (NSString *) fullName withTitle :(NSString *) title andPhoto: (UIImage *)photo;

@end
