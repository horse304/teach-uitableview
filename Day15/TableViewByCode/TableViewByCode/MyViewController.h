//
//  MyViewController.h
//  TableViewByCode
//
//  Created by techmaster on 11/24/12.
//  Copyright (c) 2012 TechMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"
@interface MyViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>  //1. Conform UIViewController to TableView's protocols
@property (nonatomic, strong) UITableView *tableView;  //2. Create a strong property to UITableView

//4. Create collection to hold Model Item
@property (nonatomic, strong) NSMutableArray *data; 
@end
