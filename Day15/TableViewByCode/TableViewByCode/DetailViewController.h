//
//  DetailViewController.h
//  TableViewByCode
//
//  Created by techmaster on 1/25/13.
//  Copyright (c) 2013 TechMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"
@interface DetailViewController : UIViewController
@property (nonatomic, weak) Person* person;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@end
